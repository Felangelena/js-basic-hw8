"use strict";

const paragraphs = document.getElementsByTagName('p');
for (let p of paragraphs) {
    p.style.backgroundColor = '#ff0000';
}

const optionsList = document.getElementById('optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);
for (let child of optionsList.childNodes) {
    console.log(child);
    console.log(`тип ${child.nodeType}`);
}

const testParagraph = document.querySelector('#testParagraph');
testParagraph.textContent = 'This is a paragraph';

const mainHeader = document.querySelector('.main-header');
for (let child of mainHeader.children) {
    child.classList.add('nav-item');
    console.log(child);
}

const sectionTitles = document.querySelectorAll('.section-title');
for (let item of sectionTitles) {
    item.classList.remove('section-title');
    console.log(item);
}

